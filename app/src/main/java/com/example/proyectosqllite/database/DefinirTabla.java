package com.example.proyectosqllite.database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla() {
    }

    public static abstract class Contacto implements BaseColumns {
        public static final String TABLA_NAME= "contactos";
        public static final String NOMBRE= "nombre";
        public static final String TELEFONO1= "telefono1";
        public static final String TELEFONO2= "telefono2";
        public static final String DOMICILIO= "domicilio";
        public static final String NOTAS= "notas";
        public static final String FAVORITO= "favorito";
    }
}
