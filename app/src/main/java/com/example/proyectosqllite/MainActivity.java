package com.example.proyectosqllite;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proyectosqllite.R;
import com.example.proyectosqllite.database.AgendaContactos;
import com.example.proyectosqllite.database.Contacto;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    EditText txtNombre;
    EditText txtTel;
    EditText txtTelefono2;
    EditText txtDomicilio;
    EditText txtNotas;
    CheckBox cbxFavorito;
    Contacto savedContact;
    Button   btnSalir;
    private AgendaContactos db;
    //int savedIndex;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTel = (EditText) findViewById(R.id.txtTel1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        txtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);
        db = new AgendaContactos(MainActivity.this);
        savedContact = null;


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNombre.getText().toString().equals("") || txtDomicilio.getText().toString().equals("") || txtTel.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setTelefono1(txtTel.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    nContacto.setDomicilio(txtDomicilio.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    if (cbxFavorito.isChecked()){
                        nContacto.setFavorito(1);
                    }
                    else{
                        nContacto.setFavorito(0);
                    }
                    db.openDataBase();
                    if (savedContact == null) {
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "Se agregó contacto con ID" + idx, Toast.LENGTH_SHORT).show();
                        savedContact = null;
                        limpiar();
                    }else {
                        db.UpdateContacto(nContacto,id);
                        Toast.makeText(MainActivity.this,"Se actualizó el registro " + id, Toast.LENGTH_SHORT).show();
                    }
                    db.cerrar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i,0);
            }
        });


        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

    }
    
    public void limpiar() {
        savedContact = null;
        txtNombre.setText("");
        txtTel.setText("");
        txtTelefono2.setText("");
        txtDomicilio.setText("");
        txtNotas.setText("");
        cbxFavorito.setChecked(false);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.get_ID();
            txtNombre.setText(contacto.getNombre());
            txtTel.setText(contacto.getTelefono1());
            txtTelefono2.setText(contacto.getTelefono2());
            txtDomicilio.setText(contacto.getDomicilio());
            txtNotas.setText(contacto.getNotas());
            if (contacto.getFavorito() > 0) {
                cbxFavorito.setChecked(true);
            }
        } else {
            limpiar();
        }
    }

}

